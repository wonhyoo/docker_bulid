#                                           	docker命令

#### 一、docker的安装与启动

```
① sudo yum update  --更新yum

② sudo yum install -y yum-utils device-mapper-persistent-data lvm2
--安装工具包

③sudo yum-config-manager --add-repo   http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo


④ sudo yum install docker-ce   --安装

⑤docker -v --查看版本
```

#### 二、设置ustc的镜像

```
vi /etc/docker/daemon.json

--加入以下内容
{
  "registry-mirrors":["https://docker.mirrors.ustc.edu.cn"]
}
```



#### 三、docker的启动与停止

~~~
① systemctl start docker  启动
② systemctl status docker 查看状态
③ systemctl stop docker 停止
④ systemctl enable docker 开机启动
~~~



#### 四、查看镜像-镜像相关

~~~
① docker images 查看镜像

② docekr search centos(镜像名称）  搜索镜像

③ docker pull centos：7（拉取自己版本） 拉取镜像

④ docekr rmi 镜像id 删除镜像（根据id）

⑤ docker rmi 'docker images -q' 删除所有的镜像

镜像的所有存储在 /var/lib/docker
~~~



#### 五、容器相关命令

~~~
① docker ps 查看正在运行的容器

② docker ps -a 查看所有容器

③ docker ps -l 查看最后一次运行的容器

④ docker ps -f status=exited 查看停止的容器

/******************************************************************************************************************************************创建与启动容器***********************************************************************************************/

① docker run ：
  -i ：运行容器
  -t：启动容器后会进入其命令，加入这两个参数后，容器创建就能登录进去，既分配一个伪终端
  --name：为创建的容器命名
  -v：表示目录映射关系
  -d：是守护进程运行
  -p：端口运行，前面是宿主机端口，后这是容器内映射的端口
  eg：docker run -it --name=容器的名称 镜像名称：标签 /bin/bash
      docker run -it --name=centos centos：7  /bin/bash
  
 ②退出容器
 exit 不是守护的方式 退出后就停止容器了
 ---进入守护进程
 ③docker exec -it 容器名称 /bin/bash
 --守护进程创建
 ④   docker -id --name=容器的名称 镜像名称：标签
 eg: docker -di --name=mycentos2 centos:7(laste)
  
 ⑤ docker exec mycentos2 /bin/bash
 
 ----------------容器启动停止、文件----------------------
 
 ① docker stop 容器名称（容器id）  停止容器
   docker stop mycentos
 ② docker start 容器名称（容器的id）启动容器
   docker start mycentos
 ③ docker cp 文件 容器名称：容器目录表  文件拷贝
   docker cp a.txt mycentos:/usr/local
 ④ docker cp 容器名称：容器目录  容器内的文件
   docker cp mycentos:/usr/local/a.txt
 
 
 目录挂载 
  ① docker run -id --name=mycentos3 -v /usr/local:/usr/local centos:7 新建
  
 ② docker run -di -v /usr/local:/usr/local --name=mycentos3 centos:7
 
 查看容器ip地址
 docker inspect 容器名称
 也可以
 docker inspect --format='{{.NetworkSettings.IPAddress}}' mycentos（容器名称）

 查看docker 宿主机的路径地址
 docker inspect --format='{{.GraphDriver.Data.MergedDir}}' 容器 ID 或名字

 
 ③docker rm mycentos3（删除容器） 正在运行的容器无法删除/镜像也是 
 
 查看已经挂载的信息
 docker inspect tomcat8 | grep Mounts -A 20

 
 
~~~



#### 五、应用部署

##### 5.1MySQL部署


~~~
① docker pull centos/mysql-57-centos7  拉取镜像

② docker run -di --name=mysql57 -p 3306:33606 -e MYSQL_ROOT_PASSWORD=123456 mysql(镜像的名称)  创建容器 指定root的密码

③ 为mysql创建用户
  1.docker exec -it mysql57(容器名) /bin/bash
  2.mysql -uroot -p123456 进入mysql控制台

 1. create user 'test'@'%' identified by '123456'; 
 2. rename user 'test'@'localhost' to 'test'@'*';
 3. flush privileges;
 4. drop user 'username'@'localhost';
 5.GRANT ALL PRIVILEGES ON  godb.* TO 'test'@'%'; -- 将某个数据库给用户访问
 ---  第一步    /*host="localhost"为本地登录用户，host="ip"为ip地址登录，host="%"，为外网ip登录*/
 
~~~



##### 5.2 Tomcat应用部署

~~~
① docker pull Tomcat：8-jre8  拉取tomcat8镜像
② docker run -di --name=tomcat8 -p 8080:8080 -v /usr/local/tomcat/webapps:/usr/local/tomcat/webapps tomcat:8-jre8

~~~

#### 5.3 Ngix部署

~~~
1. docker pull nginx 拉取Nginx镜像
2. docker run -di --name=dev_nginx -p 80:80  nginx(镜像名字)

没有挂载目录就只有手动移进去 /usr/share/nginx/ 默认读取的目录
或者修改 /etc/nginx/conf.d/ 下的默认配置文件
~~~

##### 5.4 Redis部署

~~~
1. docker pull redis 拉取镜像
2. docker run -di --name=redis -p 6379:6379 redis 创建启动容器

测试 window用redis的客户端连接
cmd--->redis-cli.exe -h ip  
set name hhh
get name
~~~



#### 六、迁移与备份

~~~ 
1.docker commit 容器名称 镜像名称， 将运行的容器创建成一个镜像
  docker commit mysql57 mysql_image
2. docker save -o mysql_im.tar mysql_image  将mysql_image导出压缩格式

3. docker load -i mysql_im.tar 镜像导入
~~~



#### 七、 Dockerfile的认识

 ##### 7.1 Dockerfile命令

| 命令                          | 作用                 |
| --------------------------- | ------------------ |
| FROM image_name:tag         | 定义使用个哪个镜           |
| MAINTAINER user_name        | 作者                 |
| ENV key value               | 设置环境变量             |
| RUN command                 | 是DockerFiile的核心部分  |
| ADD src_dir/file dest/file  | 将宿主机的文件，如果是压缩会自动解压 |
| COPY src_dir/file dest/file | 和ADD相似，有压缩文件并不能压缩  |
| WORKDIR path_dir            | 设置工作目录             |
|                             |                    |
|                             |                    |

##### 7.2构建镜像

~~~
docker build -t='image_namee' /usr/local (Dockerfile的目录)
~~~





#### 八、Docker私有仓库

（1）拉取私有仓库镜像

~~~
docker pull registry
~~~

  (2) 启动私有仓库

~~~
docker run -di --name==dockerhub -p 5000:5000 registry
~~~

 (3)访问http://ip:port/v2/_catalog 可以看到私有仓库的镜像名字

 (4) 修改daemon.json

~~~
vi /etc/docker/daemon.json
---增加 就可以将镜像上传到私服
"insecure-registries":["ip:port"]
~~~

 (5) 重启docker服务

~~~
systemctl restart docker 
~~~

##### 8.2上传镜像至私有仓库

 (1) 标记某个要上传的镜像

~~~
docker tag jdk_private/jdk8
~~~



  (2)上传标记的镜像

~~~
docker push jdk_private
~~~



linux批量kill
ps -ef|grep hf| grep -v grep|cut -c 9-15|xargs kill -9



 拉取
docker pull mysql:5.7

# 运行
docker run -p 3306:3306 --name mysql \
-v /mydata/mysql/log:/var/log/mysql \
-v /mydata/mysql/data:/var/lib/mysql \
-v /mydata/mysql/conf:/etc/mysql \
-e MYSQL_ROOT_PASSWORD=root \
--restart=always \
-d mysql:5.7
	# -p 主机端口:容器端口
		- 容器端口映射到主机对应端口
	# --name 容器名称
	# -v 主机目录:容器目录
		- 分别挂载log、data和conf目录到主机的mydata/mysql目录
	# -e MYSQL_ROOT_PASSWORD=密码
		- 设置root密码
	# --restart=always
		- 在重启docker时，自动启动mysql容器
	# -d 
		- 以后台方式运行


docker run --restart=always  \
-d --privileged=true \
-p 6379:6379 \
-v /usr/share/redis/docker/redis.conf:/etc/redis/redis.conf \
-v /usr/share/redis/docker/data:/data \
--name redis redis:5.0.7 redis-server \
 /etc/redis/redis.conf \
 --appendonly yes

### mysql建库
-- 建库
create database soac;

-- 创建用户
CREATE USER 'soac_user'@'%' IDENTIFIED BY 'Visizen.028';

-- 赋权限
GRANT all ON soac.* TO 'soac_user'@'%' IDENTIFIED BY 'Visizen.028';

-- 刷新权限
flush privileges;