#### 创建mysql
docker run -p 3306:3306 --name guli_mysql \
-v /opt/gulimail/log:/var/log/mysql \
-v /opt/gulimail/data:/var/lib/mysql \
-v /opt/gulimail/conf:/etc/mysql \
-e MYSQL_ROOT_PASSWORD=123456 \
-d mysql:5.7

#### 创建redis
docker run -p 6379:6379 --name guli_redis \
-v /opt/redis/data:/data \
-v /opt/redis/conf/redis.cnf:/etc/redis/redis.cnf \
-d redis redis-server /etc/redis/redis.cnf

#### 创建nacos

docker run -d --name nacos -p 8848:8848 -e PREFER_HOST_MODE=hostname -e MODE=standalone nacos/nacos-server
docker update nacos --restart=always

#### 创建Nginx
docker run -p 80:80 --name nginx \
-v /usr/local/nginx/html:/usr/share/nginx/html  \
-v /usr/local/nginx/logs:/var/log/nginx \
-v /usr/local/nginx/conf:/etc/nginx \
-d nginx:1.22
#### 更新容器
docker update <name> --restart=always
docker update guli_mysql --restart=always  ---更新容器自动重启